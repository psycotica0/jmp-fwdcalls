#!/usr/bin/env ruby
#
# Copyright (C) 2017-2020  Denver Gingerich <denver@ossguy.com>
#
# This file is part of jmp-fwdcalls.
#
# jmp-fwdcalls is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# jmp-fwdcalls is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with jmp-fwdcalls.  If not, see <http://www.gnu.org/licenses/>.

$stdout.sync = true

puts "JMP Forward Calls - for use with Catapult"
puts "==>> last commit of this version is " + `git rev-parse HEAD`
puts

puts "Copyright (C) 2017-2020 Denver Gingerich.  jmp-fwdcalls is licensed under"
puts "AGPLv3+.  You can download the Complete Corresponding Source code"
puts "from https://gitlab.com/ossguy/jmp-register ."
puts

t = Time.now
puts "LOG %d.%09d: starting...\n\n" % [t.to_i, t.nsec]

require "time"
require "uri"

require "open3"
require "securerandom"

if ARGV.size != 9 and ARGV.size != 10
	puts "Usage: jmp-fwdcalls.rb <component_jid> "\
		"<component_password> <server_hostname> "\
		"<server_port> <delivery_receipt_url> "\
		"<http_listen_port> <mms_proxy_prefix_url> "\
		"<catapult_domain> <cheogram_jid> [V1_creds_file]"
	exit! 0
end

# TODO: remove hack for sgx-bwmsgsv2
if ARGV.size == 9
	require_relative 'sgx-catapult/sgx-catapult'
else
	require_relative '../sgx-bwmsgsv2/sgx-bwmsgsv2'

	# per https://stackoverflow.com/questions/4662722/extending-a-ruby-module-in-another-module-including-the-module-methods#4662934
	module SGXcatapult
		class << self
			SGXbwmsgsv2.singleton_methods.each do |m|
				define_method m, SGXbwmsgsv2.method(m).to_proc
			end
		end
	end

	V1_CREDS = File.read(ARGV.pop.freeze).split
	if V1_CREDS.length != 3
		puts "V1_creds_file is #{V1_CREDS.length} lines; must be 3"
		exit! 1
	end
end

CHEOGRAM_JID = ARGV.pop.freeze
CATAPULT_DOMAIN = ARGV.pop.freeze

RECEIPT_URL = ARGV[4]

# TODO: remove hack for sgx-bwmsgsv2
if not RECEIPT_URL.start_with? 'http'
	RECEIPT_URL = 'http://ossguy.com:' + ARGV[5] + '/'

	t = Time.now
	puts "HACK %d.%09d: replacing delivery receipt URL '%s' with '%s'" %
				[t.to_i, t.nsec, ARGV[4], RECEIPT_URL]
else
	# TODO: delete this section once confirmed working
	t = Time.now
	puts "NOHACK %d.%09d: no replacement of URL '%s' needed so leave it" %
				[t.to_i, t.nsec, RECEIPT_URL]
end

def tai_now
	# Get "right" tzdata header
	head = open('/usr/share/zoneinfo/right/UTC').read(32)
	# Extract current leapseconds from header
	leaps = head[28..-1].unpack('N')[0]
	# TAI now = UTC + leaps + 10
	Time.now.to_i + leaps + 10
end

def uri_escape(s)
	URI.escape(s.to_s, /[^0-9a-zA-Z!\-=_*+.]/)
end

# TODO: keep in sync with jmp-acct_bot.rb, and eventually put in common location
module CatapultSettingFlagBits
	VOICEMAIL_TRANSCRIPTION_DISABLED = 0
end

# TODO: remove hack for sgx-bwmsgsv2
def v1ify(creds)
	# TODO: remove, as shouldn't happen
	return creds if creds.nil? or creds.first.nil?

	if creds.first.start_with? 'u-'
		creds
	elsif creds.length == 3
		V1_CREDS
	elsif creds.length == 4
		V1_CREDS + [creds.last]
	end
end

class JmpFwdcallsBase
	def initialize(app)
		@app = app
	end

	def call(env)
		params = env['params']
		return @app.call(env) unless handle_event?(params)

		t = Time.now
		if params.key?('cause')
			env.logger.info "%d.%09d: %s - %s - %s\n" % [
				t.to_i,
				t.nsec,
				params['callId'],
				params['eventType'],
				params['cause']
			]
		else
			env.logger.info "%d.%09d: %s - %s\n" % [
				t.to_i,
				t.nsec,
				params['callId'],
				params['eventType']
			]
		end

		if params.key?('tag') && params['tag'] != 'VOICEMAIL'
			# verification call started or done; get needed details

			cred_key = 'catapult_cred-' + params['tag']
			REDIS.lrange(cred_key, 0, 2)
		else
			EMPromise.resolve([])
		end.then { |creds|
			env['jmp.creds'] = v1ify(creds)
			response(env)
		}.catch { |e|
			EMPromise.reject(e) if e != :done
		}.then { |r|
			if r.is_a?(Array) && r.length == 3 \
			   && r[0].is_a?(Integer)
				r
			else
				[200, {}, "OK"]
			end
		}.sync
	end

	def write_and_promise(stanza)
		promise = EMPromise.new

		# TODO: make prettier - this is needed despite module hack above
		client = if SGXcatapult.respond_to?(:client, true)
			SGXcatapult.send(:client)
		else
			SGXbwmsgsv2.send(:client)
		end

		# Error after 5 seconds
		timer = EM::Timer.new(5) {
			error = Blather::Stanza::Iq.new(
				:error,
				stanza.from,
				stanza.id
			)
			client.handle_data(error)
		}

		client.write_with_handler(stanza) { |reply|
			timer.cancel
			promise.fulfill(reply)
		}

		promise
	end

	def sanitize_tel_candidate(candidate)
		if candidate.length < 3
			'13;phone-context=anonymous.phone-context.soprani.ca'
		elsif candidate[0] == '+' and /\A\d+\z/.match(candidate[1..-1])
			candidate
		elsif candidate == 'Restricted'
			'14;phone-context=anonymous.phone-context.soprani.ca'
		elsif candidate == 'anonymous'
			'15;phone-context=anonymous.phone-context.soprani.ca'
		elsif candidate == 'Anonymous'
			'16;phone-context=anonymous.phone-context.soprani.ca'
		elsif candidate == 'unavailable'
			'17;phone-context=anonymous.phone-context.soprani.ca'
		elsif candidate == 'Unavailable'
			'18;phone-context=anonymous.phone-context.soprani.ca'
		else
			'19;phone-context=anonymous.phone-context.soprani.ca'
			# TODO: add 'NONE', 'NULL', 'null', 'ANONYMOUS' (18, 7, 6, 5)?
		end
	end

	def handle_event?(*)
		false
	end

	def response(*)
		raise "You must override JmpFwdcallsBase#response in subclass"
	end
end

class JmpIncomingCall < JmpFwdcallsBase
	def handle_event?(params)
		params['eventType'] == 'incomingcall'
	end

	def fwd_call(
		logger,
		user_id, api_token, api_secret,
		params, jid, fwdphone, from_num
	)
		# convert fwdphone into the format Catapult accepts
		fwdphone.slice!(0..3) if fwdphone.start_with?('tel:')

		# create a bridge and add the incoming call to it
		EMPromise.all([
			SGXcatapult.call_catapult(
				api_token,
				api_secret,
				:post,
				"v1/users/#{user_id}/bridges",
				JSON.dump(
					bridgeAudio: 'true',
					callIds:     [params['callId']]
				),
				{'Content-Type' => 'application/json'},
				[201],
				:headers
			).catch { |e|
				logger.error(
					'to ' + params['to'] +
					' from ' + params['from'] +
					' fwding to ' + fwdphone
				)
				EMPromise.reject(e)
			},
			REDIS.get("catapult_fwd_timeout-#{jid}").promise
		]).then { |(response, timeout)|
			# TODO: confirm successful response
			bridge_id = File.basename(response['location'])
			timeout = (timeout || -1).to_i
			call = {
				from:        from_num,
				to:          fwdphone,
				bridgeId:    bridge_id,
				# TODO: add recordingEnabled if user wants it
				callbackUrl: RECEIPT_URL
			}

			if !params['from'].start_with?('sip:') and timeout > 0
				call[:callTimeout] = timeout
			end

			if !params['from'].start_with?('sip:') and timeout == 0
				JmpVoicemailHangup
					.new(nil)
					.response(
						{'params' => {}},
						params['callId'],
						jid
					).catch(&method(:panic))

				# Above will run in background while we
				# continue to set the faux cid
				{'location' => SecureRandom.uuid}
			else
				# call the user's fwdphone, adding it to the bridge
				SGXcatapult.call_catapult(
					api_token,
					api_secret,
					:post,
					"v1/users/#{user_id}/calls",
					JSON.dump(call),
					{'Content-Type' => 'application/json'},
					[201],
					:headers
				)
			end
		}.catch { |e|
			logger.error 'from ' + params['from'] + ' to ' +
				params['to'] + ' forwarding to ' + fwdphone

			EMPromise.reject(e)
		}.then { |response|
			# TODO: confirm successful response; error if not
			loc_call_id = File.basename(response['location'])

			EMPromise.all([
				# JID stays around for two days, call mappings for 1 day
				REDIS.setex(
					"bri-call_jid-#{loc_call_id}",
					172800,
					jid
				),
				# SET in this order to reduce chance of call hangup race
				# first we link the incoming call to the forwarding call
				REDIS.setex(
					"bri-to_fwdnum-#{params['callId']}",
					86400,
					loc_call_id
				),
				REDIS.setex(
					"bri-to_jmpnum-#{loc_call_id}",
					86400,
					params['callId']
				)
			])
		}
	end

	def escape_jid(localpart)
		# TODO: proper XEP-0106 Sec 4.3, ie. pre-escaped
		localpart
			.gsub("\\", "\\\\5c")
			.gsub(' ', "\\\\20")
			.gsub('"', "\\\\22")
			.gsub('&', "\\\\26")
			.gsub("'", "\\\\27")
			.gsub('/', "\\\\2f")
			.gsub(':', "\\\\3a")
			.gsub('<', "\\\\3c")
			.gsub('>', "\\\\3e")
			.gsub('@', "\\\\40")
	end

	def response(env)
		params = env['params']

		if params['from'].start_with?('sip:')
			# JMP user making a call -> connect with callee
			env.logger.info 'TYPE: outgoing call from SIP'

			from_parts = params['from'].split(/@/, 2)

			if from_parts[1] != CATAPULT_DOMAIN + '.bwapp.bwsip.io'
				env.logger.error 'incoming SIP call from '\
					'unexpected domain - cannot '\
					'complete call from ' +
					params['from'] + ' to ' +
					params['to']
				return [
					500,
					{'Content-Type' => 'text/plain'},
					'invalid source'
				]
			end

			# trim leading "sip:", switch to percent, decode
			noncheo_jid = URI.unescape(
				from_parts[0][4..-1].tr('~', '%')
			)

			EMPromise.resolve([
				"#{escape_jid(noncheo_jid)}@#{CHEOGRAM_JID}",
				params['to']
			])
		else
			# someone calling user's JMP number -> call user
			env.logger.info 'TYPE: incoming call to JMP number'

			jid_key = 'catapult_jid-' + params['to']
			fwd_key = 'catapult_fwd-' + params['to']
			REDIS.mget(jid_key, fwd_key).then { |(jid, fwdphone)|
				# TODO: params['to'] if user wants it
				[jid, fwdphone, params['from']]
			}
		end.then { |(jid, fwdphone, from_num)|
			if fwdphone.nil? || fwdphone.empty?
				faux_cid = SecureRandom.uuid

				REDIS.set(
					"bri-to_fwdnum-#{params['callId']}",
					faux_cid
				).then {
					REDIS.set(
						"bri-call_jid-#{faux_cid}",
						jid
					)
				}.then {
					JmpVoicemailHangup
						.new(nil)
						.response(
							{'params' => {}},
							params['callId'],
							jid
						)
				}
			else
				cred_key = 'catapult_cred-' + jid
				REDIS.lrange(cred_key, 0, 3).then { |creds|
					if from_num.nil?
						# JMP user making call
						# using SIP account
						from_num = creds.last
					end

					fwd_call(
						env.logger,
						*v1ify(creds)[0..-2],
						params, jid, fwdphone, from_num
					)
				}
			end
		}
	end
end

class JmpRecordVoicemailGreeting < JmpFwdcallsBase
	@calls = {}

	def self.new_call(fwd, user_id, token, secret, user_num, jid)
		SGXcatapult.call_catapult(
			token,
			secret,
			:post,
			"v1/users/#{user_id}/calls",
			JSON.dump(
				from:        user_num,
				to:          fwd,
				callbackUrl: RECEIPT_URL
			),
			{'Content-Type' => 'application/json'},
			[201],
			:headers
		).then { |response|
			@calls[File.basename(response['location'])] =
				[user_id, token, secret, jid]
		}
	end

	def self.get_call(call_id)
		@calls[call_id]
	end

	def self.rm_call(call_id)
		@calls.delete(call_id)
	end

	def handle_event?(params)
		self.class.get_call(params['callId'])
	end

	def response(env)
		params = env['params']
		user_id, token, secret, jid = handle_event?(params)

		if params['eventType'] == 'answer'
			SGXcatapult.call_catapult(
				token,
				secret,
				:post,
				"v1/users/#{user_id}/calls/"\
					"#{params['callId']}/audio",
				JSON.dump(
					sentence:
						'At the tone, say your new '\
						'voicemail greeting.  Then, '\
						'hang up.'
				),
				'Content-Type' => 'application/json'
			)
			# TODO: confirm successful response; error if no
		elsif params['eventType'] == 'speak' &&
		      params['status'] == 'done'
			SGXcatapult.call_catapult(
				token,
				secret,
				:post,
				"v1/users/#{user_id}/calls/"\
					"#{params['callId']}/audio",
				JSON.dump(
					tag: 'VOICEMAIL',
					fileUrl: "https://api.catapult.inetwork.com/v1/users/" \
					         "#{user_id}/media/20170524-beep.mp3"
				),
				'Content-Type' => 'application/json'
			)
			# TODO: confirm successful response; error if no
		elsif params['eventType'] == 'playback' &&
		      params['status'] == 'done'
			SGXcatapult.call_catapult(
				token,
				secret,
				:post,
				"v1/users/#{user_id}/calls/"\
					"#{params['callId']}",
				JSON.dump(recordingEnabled: true),
				'Content-Type' => 'application/json'
			)
			# TODO: confirm successful response; error if no
		elsif params['eventType'] == 'recording' &&
		      params['status'] == 'complete'
			EM::HttpRequest.new(
				params['recordingUri']
			).get(
				head: {
					'Authorization' => [
						token, secret
					]
				}
			).then { |http|
				recording = JSON.parse(http.response)
				REDIS.set(
					"catapult_ogm_url-#{jid}",
					recording['media']
				)
			}.then {
				self.class.rm_call(params['callId'])
			}
		end
	end
end

class JmpAnswerVerificationCall < JmpFwdcallsBase
	def handle_event?(params)
		params['eventType'] == 'answer' && params.key?('tag')
	end

	def response(env)
		params = env['params']
		return if params['tag'] == 'VOICEMAIL'

		user_id, api_token, api_secret = *env['jmp.creds']

		# user picked up verification call; say the code

		pcode_key = 'reg-pcode-' + params['to']
		REDIS.get(pcode_key).then { |pcode|
			# insert break between each char of 8-char pcode
			pcode.gsub!(
				/^(.)(.)(.)(.)(.)(.)(.)(.)$/,
				'; \1; \2; \3; \4; \5; \6; \7; \8; '
			)
			say = "Your JMP verification code is #{pcode}" * 4

			SGXcatapult.call_catapult(
				api_token,
				api_secret,
				:post,
				"v1/users/#{user_id}/calls/"\
					"#{params['callId']}/audio",
				JSON.dump(
					sentence: say,
					voice: 'julie',
					tag: params['tag']
				),
				'Content-Type' => 'application/json'
			)
			# TODO: confirm successful response; error if no
		}
	end
end

class JmpAnswerIncomingCall < JmpFwdcallsBase
	def handle_event?(params)
		params['eventType'] == 'answer' && !params.key('tag')
	end

	def response(env)
		params = env['params']

		# user picked up incoming call to their JMP num

		cal_key = 'bri-call_jid-' + params['callId']
		brj_key = 'bri-to_jmpnum-' + params['callId']
		REDIS.mget(cal_key, brj_key).then { |(jid, to_jmpnum_callid)|
			# do nothing when caller to JMP answers
			next EMPromise.reject(:done) if jid.nil?

			cred_key = 'catapult_cred-' + jid
			REDIS.lrange(cred_key, 0, 2).then { |creds|
				[to_jmpnum_callid] + v1ify(creds)
			}
		}.then { |(to_jmpnum_callid, user_id, api_token, api_secret)|
			# pickup the other side of the call so connected
			SGXcatapult.call_catapult(
				api_token,
				api_secret,
				:post,
				"v1/users/#{user_id}/calls/#{to_jmpnum_callid}",
				JSON.dump(state: 'active'),
				'Content-Type' => 'application/json'
			)
			# TODO: confirm successful response; error if no
		}
	end
end

class JmpRecording < JmpFwdcallsBase
	def self.callids
		@callids ||= {}
	end

	def handle_event?(params)
		params['eventType'] == 'recording'
	end

	def response(env)
		params = env['params']
		JmpRecording.callids[params['recordingId']] = params['callId']
		return unless params['status'] == 'complete'

		user_id = ''
		token = ''
		secret = ''

		REDIS.get('bri-to_fwdnum-' + params['callId']).then { |call_id|
			REDIS.get('bri-call_jid-' + call_id)
		}.then { |jid|
			cred_key = 'catapult_cred-' + jid
			REDIS.lrange(cred_key, 0, 2).then { |creds|
				[jid] + v1ify(creds)
			}
		}.then { |(jid, p_user_id, p_token, p_secret)|
			user_id = p_user_id
			token = p_token
			secret = p_secret
			EMPromise.all([
				EMPromise.resolve(jid),
				SGXcatapult.call_catapult(
					token,
					secret,
					:get,
					"v1/users/#{user_id}/calls/"\
						"#{params['callId']}",
					""
				),
				EM::HttpRequest.new(
					params['recordingUri']
				).get(
					head: {
						'Authorization' => [
							token, secret
						]
					}
				).promise
			])
		}.then { |(jid, call_info, http)|
			# TODO: check for error response
			JSON
				.parse(call_info)
				.merge(JSON.parse(http.response))
				.merge(jid: jid)
		}.then { |meta|
			duration = Time.parse(meta['endTime']) - \
			           Time.parse(meta['startTime'])
			# Ignore very short voicemails (<5 seconds)
			next unless duration > 5

			sanitized_from = sanitize_tel_candidate(meta['from'])

			description = 'Voicemail Recording'
			if sanitized_from != meta['from']
				description = 'Voicemail Recording from "' +
					meta['from'] + '"'
			end

			# TODO: meta['displayName'] could be sent as <nick/>
			SGXcatapult.send_media(
				"#{sanitized_from}@#{ARGV[0]}",
				meta[:jid],
				meta['media'],
				description,
				'New Voicemail'
			)
		}.then {
			# hangup call when recording is done
			SGXcatapult.call_catapult(
				token,
				secret,
				:post,
				"v1/users/#{user_id}/calls/#{params['callId']}",
				JSON.dump(state: 'completed'),
				'Content-Type' => 'application/json'
			)
		}
	end
end

class JmpTranscription < JmpFwdcallsBase
	def handle_event?(params)
		params['eventType'] == 'transcription'
	end

	def response(env)
		params = env['params']
		return unless params['status'] == 'completed'

		params['callId'] = JmpRecording.callids.delete(
			params['recordingId']
		)

		REDIS.get('bri-to_fwdnum-' + params['callId']).then { |call_id|
			REDIS.get('bri-call_jid-' + call_id)
		}.then { |jid|
			cred_key = 'catapult_cred-' + jid
			REDIS.lrange(cred_key, 0, 2).then { |creds|
				[jid] + v1ify(creds)
			}
		}.then { |(jid, user_id, token, secret)|
			EMPromise.all([
				EMPromise.resolve(jid),
				SGXcatapult.call_catapult(
					token,
					secret,
					:get,
					"v1/users/#{user_id}/calls/"\
						"#{params['callId']}",
					""
				),
				EM::HttpRequest.new(
					params['textUrl']
				).get(
					head: {
						'Authorization' => [
							token, secret
						]
					}
				).promise
			])
		}.then { |(jid, call_info, http)|
			# TODO: check for error response
			JSON
				.parse(call_info)
				.merge(text: http.response)
				.merge(jid: jid)
		}.then { |meta|
			next if meta[:text].to_s.empty?

			# TODO: meta['displayName'] could be sent as <nick/>

			sanitized_from = sanitize_tel_candidate(meta['from'])

			# need preface since Conversations hides subject
			body = 'Voicemail Transcription: ' + meta[:text]
			if sanitized_from != meta['from']
				body = 'Voicemail Transcription from "' +
					meta['from'] + '": ' + meta[:text]
			end

			msg = Blather::Stanza::Message.new(
				meta[:jid],
				body
			)
			msg.from = "#{sanitized_from}@#{ARGV[0]}"
			msg.subject = 'Voicemail Transcription'
			SGXcatapult.write(msg)
		}
	end
end

class JmpVoicemailSpeak < JmpFwdcallsBase
	def handle_event?(params)
		params['eventType'] == 'speak' && params['tag'] == 'VOICEMAIL'
	end

	def response(env)
		params = env['params']
		return unless params['status'] == 'done'

		REDIS.get('bri-to_fwdnum-' + params['callId']).then { |call_id|
			REDIS.get('bri-call_jid-' + call_id)
		}.then { |jid|
			EMPromise.all([
				REDIS.getbit(
					'catapult_setting_flags-' + jid,
					CatapultSettingFlagBits::VOICEMAIL_TRANSCRIPTION_DISABLED
				).then { |x| x != 1 },
				REDIS.lrange('catapult_cred-' + jid, 0, 2)
			])
		}.then { |(transcription_enabled, creds)|
			user_id, api_token, api_secret = v1ify(creds)
			SGXcatapult.call_catapult(
				api_token,
				api_secret,
				:post,
				"v1/users/#{user_id}/calls/"\
					"#{params['callId']}/audio",
				JSON.dump(
					fileUrl: "https://api.catapult.inetwork.com/v1/users/" \
				            "#{user_id}/media/20170524-beep.mp3"
				),
				'Content-Type' => 'application/json'
			).then {
				SGXcatapult.call_catapult(
					api_token,
					api_secret,
					:post,
					"v1/users/#{user_id}/calls/"\
						"#{params['callId']}",
					JSON.dump(
						recordingEnabled: true,
						# recordings at most 11 minutes
						recordingMaxDuration: 630,
						transcriptionEnabled: transcription_enabled,
						tag: 'VOICEMAIL'
					),
					'Content-Type' => 'application/json'
				)
			}
		}.catch { |e|
			# 403 can happen when the call is already hung-up
			next if e == 403
			EMPromise.reject(e)
		}
	end
end

class JmpSpeak < JmpFwdcallsBase
	def handle_event?(params)
		params['eventType'] == 'speak'
	end

	def response(env)
		params = env['params']
		user_id, api_token, api_secret = *env['jmp.creds']

		# on other status (ie. starting or similar) do nothing
		return if params['status'] != 'done'

		# hangup call when code or "no voice" audio done
		SGXcatapult.call_catapult(
			api_token,
			api_secret,
			:post,
			"v1/users/#{user_id}/calls/#{params['callId']}",
			JSON.dump(state: 'completed'),
			'Content-Type' => 'application/json'
		)
		# TODO: confirm successful response; error if no
	end
end

class JmpPlayback < JmpFwdcallsBase
	def handle_event?(params)
		params['eventType'] == 'playback'
	end

	def response(env)
		params = env['params']
		return unless params['tag'] == 'VOICEMAIL'
		return unless params['status'] == 'done'
		JmpVoicemailSpeak.new(nil).response(env)
	end
end

class JmpVoicemailHangup < JmpFwdcallsBase
	def handle_event?(params)
		params['eventType'] == 'hangup' && params['tag'] == 'VOICEMAIL'
	end

	def jmpnum_callid(brj_callid, jmpnum_callid)
		return EMPromise.resolve(jmpnum_callid) if jmpnum_callid

		REDIS.get("bri-to_jmpnum-#{brj_callid}").then { |call_id|
			# Other side hung up
			next EMPromise.reject(:done) unless call_id
			call_id
		}
	end

	def tts_greeting(jid, call_id, user_id, api_token, api_secret)
		t = Time.now
		puts "VCARDGET %d.%09d: for JID '%s'" % [t.to_i, t.nsec, jid]

		iq = Blather::Stanza::Iq::Vcard.new(:get, jid)
		write_and_promise(iq).then { |reply|
			next unless reply.respond_to?(:vcard)
			reply.vcard['FN'] || reply.vcard['NICKNAME']
		}.then { |fn|
			if fn and not fn.empty?
				t = Time.now
				puts "TTS %d.%09d: needed for fn '%s', j '%s'" %
					[t.to_i, t.nsec, fn, jid]
				fn
			else
				puts "FNEMPTY for '#{jid}'" if fn

				t = Time.now
				puts "NOTTS %d.%09d: no TTS needed for j '%s'" %
					[t.to_i, t.nsec, jid]
				"a user of JMP dot chat"
			end
		}.then { |fn|
			SGXcatapult.call_catapult(
				api_token,
				api_secret,
				:post,
				"v1/users/#{user_id}/calls/#{call_id}",
				JSON.dump(state: 'active'),
				'Content-Type' => 'application/json'
			).then {
				# TODO: confirm successful response; error if no
				say = "You have reached the voicemail of "\
					"#{fn}. Please send a text message, "\
					"or leave a message after the tone."
				SGXcatapult.call_catapult(
					api_token,
					api_secret,
					:post,
					"v1/users/#{user_id}/calls/"\
						"#{call_id}/audio",
					JSON.dump(
						tag: 'VOICEMAIL',
						sentence: say
					),
					'Content-Type' => 'application/json'
				)
				# TODO: confirm successful response; error if no
			}
		}
	end

	def media_greeting(media, call_id, user_id, api_token, api_secret)
		SGXcatapult.call_catapult(
			api_token,
			api_secret,
			:post,
			"v1/users/#{user_id}/calls/#{call_id}",
			JSON.dump(state: 'active'),
			'Content-Type' => 'application/json'
		).then {
			# TODO: confirm successful response; error if no
			SGXcatapult.call_catapult(
				api_token,
				api_secret,
				:post,
				"v1/users/#{user_id}/calls/"\
					"#{call_id}/audio",
				JSON.dump(
					tag: 'VOICEMAIL',
					fileUrl: media
				),
				'Content-Type' => 'application/json'
			)
			# TODO: confirm successful response; error if no
		}
	end

	def response(env, jmpnum_callid=nil, known_jid=nil)
		params = env['params']

		jmpnum_callid(params['callId'], jmpnum_callid).then { |call_id|
			if known_jid
				EMPromise.resolve(known_jid)
			else
				REDIS.get("bri-call_jid-#{params['callId']}")
			end.then { |jid|
				cred_key = 'catapult_cred-' + jid
				REDIS.lrange(cred_key, 0, 2).then { |creds|
					[jid, call_id] + v1ify(creds)
				}
			}
		}.then { |jid, *creds|
			REDIS.get("catapult_ogm_url-#{jid}").then { |ogm_url|
				if ogm_url
					media_greeting(ogm_url, *creds)
				else
					tts_greeting(jid, *creds)
				end
			}
		}
	end
end

class JmpHangup < JmpFwdcallsBase
	def handle_event?(params)
		params['eventType'] == 'hangup'
	end

	NORMAL_HANGUP = ['NORMAL_CLEARING', 'ORIGINATOR_CANCEL'].freeze

	def response(env)
		params = env['params']

		# user hungup verification code, "no voice", or fwd
		#  call, or caller to JMP hung up

		state = if NORMAL_HANGUP.include?(params['cause'])
			'completed'
		else
			'rejected'
		end

		brf_key = 'bri-to_fwdnum-' + params['callId']
		brj_key = 'bri-to_jmpnum-' + params['callId']
		REDIS.mget(brf_key, brj_key).then { |(fcallid, jcallid)|
			if fcallid
				# call that was hungup is call to the JMP number
				[
					"bri-call_jid-#{fcallid}",
					fcallid
				]
			elsif jcallid && state == 'rejected'
				REDIS.get(
					"bri-call_jid-#{params['callId']}"
				).then { |jid|

					cred_key = 'catapult_cred-' + jid
					REDIS.lrange(cred_key, 3, 3)
				}.then { |ljmpnum|
					if params['from'] == ljmpnum[0]
						# outgoing from JMP; pass reject
						[
							"bri-call_jid-#{params['callId']}",
							jcallid
						]
					else
						# fwd num rejected call so -> VM
						next EMPromise.reject(
							[:voicemail, jcallid]
						)
					end
				}
			elsif jcallid
				# call that was hungup is call to the fwd number
				[
					"bri-call_jid-#{params['callId']}",
					jcallid
				]
			else
				# user hungup code or "no voice" call
				# TODO: slight chance of incomingcall race?
				# could check tag to be sure
				# (if has no tag, it's race)
				EMPromise.reject(:done)
			end
		}.then { |(cal_key, callid_to_hangup)|

			creds = []
			t = ''
			tai_timestamp = ''
			tai_yyyymmdd = ''

			REDIS.get(cal_key).then { |jid|
				cred_key = 'catapult_cred-' + jid
				REDIS.lrange(cred_key, 0, 3)
			}.then { |elements|
				creds = v1ify(elements)

				# hangup the other side,
				# since this side hungup
				SGXcatapult.call_catapult(
					creds[1],
					creds[2],
					:post,
					"v1/users/#{creds.first}/calls/"\
						"#{callid_to_hangup}",
					JSON.dump(state: state),
					'Content-Type' => 'application/json'
				)
			}.then {
				t = Time.now
				tai_timestamp = tai_now
				tai_yyyymmdd = Time.at(tai_timestamp).utc.strftime('%Y%m%d')

				SGXcatapult.call_catapult(
					creds[1],
					creds[2],
					:get,
					"v1/users/#{creds.first}/calls/" +
						params['callId']
				)
			}.then { |call_response|
				call_details = JSON.parse(call_response)
				minutes = 0

				if call_details['to'] == creds[3] or
					call_details['from'] == creds[3]

					minutes = call_details[
						'chargeableDuration'].to_i / 60
				end

				puts "SMU %d.%09d, %s: %d min for %s on %s\n" %
					[t.to_i, t.nsec, tai_timestamp, minutes,
						creds[3], tai_yyyymmdd]

				REDIS.incrby('usage_minutes-' + tai_yyyymmdd +
					'-' + creds[3], minutes)
			}.then { |total|
				t = Time.now
				puts "SMU %d.%09d: mins for %s-%s now at %s\n" %
					[t.to_i, t.nsec, tai_yyyymmdd, creds[3],
						total]
			}
			# TODO: confirm successful response; error if no
		}.catch { |e|
			# 403 in the wild sometimes (c-3e6j...), can't reproduce
			next if e == 403

			if e.is_a?(Array) && e.first == :voicemail
				JmpVoicemailHangup
					.new(nil)
					.response(env, e.last)
			else
				EMPromise.reject(e)
			end
		}
	end
end

class JmpTimeout < JmpFwdcallsBase
	def handle_event?(params)
		params['eventType'] == 'timeout'
	end

	def response(env)
		params = env['params']

		jid_key = "bri-call_jid-#{params['callId']}"
		REDIS.get(jid_key).then { |jid|
			cred_key = "catapult_cred-#{jid}"
			REDIS.lrange(cred_key, 0, 2)
		}.then { |creds|
			user_id, api_token, api_secret = v1ify(creds)
			SGXcatapult.call_catapult(
				api_token,
				api_secret,
				:post,
				"v1/users/#{user_id}/calls/#{params['callId']}",
				JSON.dump(state: 'completed', tag: 'VOICEMAIL'),
				'Content-Type' => 'application/json'
			)
			# TODO: confirm successful response; error if no
		}
	end
end

class JmpDTMF < JmpFwdcallsBase
	def handle_event?(params)
		params['eventType'] == 'dtmf'
	end

	def response(env)
		params = env['params']

		if params.key?('dtmfDigit')
			env.logger.info "user pressed '#{params['dtmfDigit']}'"
		else
			env.logger.info 'DTMF event received with no digit'
		end
	end
end

class JmpError < JmpFwdcallsBase
	def handle_event?(params)
		params['eventType'] == 'error'
	end

	def response(env)
		params = env['params']

		if params.key?('callState')
			env.logger.info "ERR callState '#{params['callState']}'"
		else
			env.logger.info 'ERROR received with no callState'
		end
	end
end

class JmpEmpty < JmpFwdcallsBase
	def handle_event?(params)
		not params.key?('eventType') or params['eventType'] == ''
	end

	def response(env)
		params = env['params']

		if params.key?('eventType')
			env.logger.info 'EMPTY eventType message received'
		else
			env.logger.info 'MISSING eventType message received'
		end
	end
end

WebhookHandler.send(:use, JmpIncomingCall)
WebhookHandler.send(:use, JmpRecordVoicemailGreeting)
WebhookHandler.send(:use, JmpAnswerVerificationCall)
WebhookHandler.send(:use, JmpAnswerIncomingCall)
WebhookHandler.send(:use, JmpRecording)
WebhookHandler.send(:use, JmpTranscription)
WebhookHandler.send(:use, JmpVoicemailSpeak)
WebhookHandler.send(:use, JmpSpeak)
WebhookHandler.send(:use, JmpPlayback)
WebhookHandler.send(:use, JmpVoicemailHangup)
WebhookHandler.send(:use, JmpHangup)
WebhookHandler.send(:use, JmpTimeout)
WebhookHandler.send(:use, JmpDTMF)
WebhookHandler.send(:use, JmpError)
#WebhookHandler.send(:use, JmpEmpty) # TODO: reinstate error handler, fix for V2

CMD_NS = 'http://jabber.org/protocol/commands'.freeze
SGXcatapult.add_gateway_feature(CMD_NS)
SGXcatapult.before_handler(:disco_items, node: CMD_NS) do |iq|
	throw :pass if iq.to.node

	cred_key = "catapult_cred-#{iq.from.stripped}"
	REDIS.lrange(cred_key, 3, 3).then { |(unum)|
		fwd_key = "catapult_fwd-#{unum}"
		REDIS.get(fwd_key).then { |fwd| [unum, fwd] }
	}.then { |unum, fwd|
		reply = iq.reply
		reply.node = CMD_NS

		if unum
			reply.items = [
				Blather::Stanza::DiscoItems::Item.new(
					iq.to,
					'number-display',
					'Display JMP Number'
				)
			] + [
				Blather::Stanza::DiscoItems::Item.new(
					iq.to,
					'usage',
					'Show Monthly Usage'
				)
			] + [
				Blather::Stanza::DiscoItems::Item.new(
					iq.to,
					'reset-sip-account',
					'Reset SIP Account (if forgot password)'
				)
			] + [
				Blather::Stanza::DiscoItems::Item.new(
					iq.to,
					'configure-calls',
					'Configure Calls'
				)
			] + (fwd ? [
				Blather::Stanza::DiscoItems::Item.new(
					iq.to,
					'record-voicemail-greeting',
					'Record Voicemail Greeting'
				)
			] : [])
		end

		SGXcatapult.write reply
	}
end

def xep0122_field(type, field)
	f = Blather::Stanza::X::Field.new(field)
	validate = Nokogiri::XML::Node.new('validate', f.document)
	validate['xmlns'] = 'http://jabber.org/protocol/xdata-validate'
	validate['datatype'] = type
	f.add_child(validate)
	f
end

def xep0004_boolean_value(field)
	['1', 'true'].include?(field.value.to_s.downcase)
end

SGXcatapult.before_handler(
	:command,
	:execute?,
	node: 'number-display',
	sessionid: nil
) do |iq|
	throw :pass if iq.to.node

	cred_key = "catapult_cred-#{iq.from.stripped}"
	REDIS.lrange(cred_key, 0, 3).then { |elements|
		creds = v1ify(elements)

		reply = iq.reply
		reply.command[:sessionid] = iq.sessionid
		reply.node = 'number-display'
		reply.status = :completed
		note = reply.note
		note.content = "Your JMP number is " + creds.last
		note[:type] = :info

		SGXcatapult.write reply
	}
end

SGXcatapult.before_handler(
	:command,
	:execute?,
	node: 'usage',
	sessionid: nil
) do |iq|
	throw :pass if iq.to.node

	start_day = ''
	date_list = []
	count = 0

	cred_key = "catapult_cred-#{iq.from.stripped}"
	REDIS.lindex(cred_key, 3).then { |tel|
		EMPromise.all([
			tel,
			REDIS.get("usage_start_day-#{tel}")
		])
	}.then { |(tel, usage_start_day)|
		start_day = usage_start_day

		if not start_day or start_day.length < 8
			reply = iq.reply
			reply.command[:sessionid] = iq.sessionid
			reply.node = 'usage'
			reply.status = :completed
			reply.note.content = "Unable to get usage statistics"
			reply.note[:type] = :info

			SGXcatapult.write reply

			t = Time.now
			puts "PERR %d.%09d: no stats for %s (%s) - startd: %s" %
				[t.to_i, t.nsec, tel, iq.from.stripped, start_day]

			next EMPromise.reject(:done)
		end

		usage_keys = []

		d = Date.today
		d_str = d.strftime('%Y%m%d')
		while d_str >= start_day and count < 30
			date_list.push(d_str)
			usage_keys.push('usage_minutes-' + d_str + '-' + tel)
			usage_keys.push('usage_messages-' + d_str + '-' + tel)
			d = d.prev_day
			d_str = d.strftime('%Y%m%d')
			count += 1
		end

		EMPromise.all([
			tel,
			REDIS.mget(*usage_keys)
		])
	}.then { |(tel, usage_values)|

		reply = iq.reply
		reply.command[:sessionid] = iq.sessionid
		reply.node = 'usage'
		reply.status = :completed
		note = reply.note

		note.content = if date_list.last == start_day
			"Usage since you signed up on #{start_day}\n"
		else
			"Usage statistics for the past 30 days\n"
		end
		note.content += "(minutes, outgoing messages):\n\n"

		total_mins = 0
		total_msgs = 0

		0.upto(count-1) do |i|
			offset = 0
			separator = "\t"
			if i.odd?
				offset = (count + 1) / 2
				separator = "\n"
			end

			mins = usage_values[2 * (i / 2 + offset)] ?
				usage_values[2 * (i / 2 + offset)].to_i : 0
			msgs = usage_values[2 * (i / 2 + offset) + 1] ?
				usage_values[2 * (i / 2 + offset) + 1].to_i : 0

			total_mins += mins
			total_msgs += msgs

			note.content += date_list[i / 2 + offset] + ': ' +
				mins.to_s + ', ' + msgs.to_s + separator
		end

		note.content += "\nTotal minutes: " + total_mins.to_s +
			', total outgoing messages: ' + total_msgs.to_s + "\n\n"
		note[:type] = :info

		SGXcatapult.write reply

		t = Time.now
		puts "PMU %d.%09d: past %s day usage for %s: %s mins, %s msgs" %
			[t.to_i, t.nsec, count, tel, total_mins.to_s,
				total_msgs.to_s]
	}
end

SGXcatapult.before_handler(
	:command,
	:execute?,
	node: 'reset-sip-account',
	sessionid: nil
) do |iq|
	throw :pass if iq.to.node

	creds = []
	domain_id = ''
	endpoint_id = ''
	application_id = ''

	# TODO: proper XEP-0106 Sec 4.3, ie. pre-escaped
	user_jid =
		iq
		.from.to_s.split('@', 2)[0]
		.gsub("\\20", ' ')
		.gsub("\\22", '"')
		.gsub("\\26", '&')
		.gsub("\\27", "'")
		.gsub("\\2f", '/')
		.gsub("\\3a", ':')
		.gsub("\\3c", '<')
		.gsub("\\3e", '>')
		.gsub("\\40", '@')
		.gsub("\\5c", "\\\\")

	user = uri_escape(user_jid).tr('%', '~')

	# create password with https://github.com/singpolyma/mnemonicode
	stdin, stdout, = Open3.popen3('./mnencode')
	# note that Catapult only allows passwords up to 25 chars so...
	stdin.print(SecureRandom.random_bytes(4))
	stdin.close
	new_pw = stdout.gets.strip

	cred_key = "catapult_cred-#{iq.from.stripped}"
	REDIS.lrange(cred_key, 0, 3).then { |elements|
		creds = v1ify(elements)

		SGXcatapult.call_catapult(
			creds[1],
			creds[2],
			:get,
			"v1/users/#{creds.first}/domains"
		)
	}.then { |domains_response|
		JSON.parse(domains_response).each do |domain|
			if CATAPULT_DOMAIN == domain['name']
				domain_id = domain['id']
				break
			end
		end

		SGXcatapult.call_catapult(
			creds[1],
			creds[2],
			:get,
			# TODO: need to iterate over not just first thousand
			"v1/users/#{creds.first}/domains/" + domain_id +
				"/endpoints?size=1000"
		)
	}.then { |endpoints_response|
		JSON.parse(endpoints_response).each do |endpoint|
			if user == endpoint['name']
				endpoint_id = endpoint['id']
				application_id = endpoint['applicationId']
				break
			end
		end

		SGXcatapult.call_catapult(
			creds[1],
			creds[2],
			:delete,
			"v1/users/#{creds.first}/domains/" + domain_id +
				'/endpoints/' + endpoint_id
		)
	}.then {
		# TODO: confirm deletion was successful using argument

		SGXcatapult.call_catapult(
			creds[1],
			creds[2],
			:post,
			"v1/users/#{creds.first}/domains/" + domain_id +
				'/endpoints',
			JSON.dump(
				name:		user,
				applicationId:	application_id,
				credentials:	{password: new_pw}
			),
			'Content-Type' => 'application/json'
		)
		# TODO: confirm creation was successful

		reply = iq.reply
		reply.command[:sessionid] = iq.sessionid
		reply.node = 'reset-sip-account'
		reply.status = :completed
		note = reply.note
		note.content = "SIP account reset - your new credentials:\n\n" \
			"server: #{CATAPULT_DOMAIN}.bwapp.bwsip.io\n" \
			"user ID: #{user}\n" \
			"password: #{new_pw}"
		note[:type] = :info

		SGXcatapult.write reply
	}
end

def fwd_defaults_for(jid)
	# remove @cheogram.com (which we should not know about, but leaks here)
	# replace existing jid escaping with ~ escaping for bandwidth username
	bw_user = jid.to_s.split('@', 2)[0].tr('\\', '~')
	[
		"sip:#{bw_user}@jmp.bwapp.bwsip.io",
		"sip:#{uri_escape(jid)}@sip.cheogram.com"
	]
end

SGXcatapult.before_handler(
	:command,
	:execute?,
	node: 'configure-calls',
	sessionid: nil
) do |iq|
	throw :pass if iq.to.node

	default_fwd, jingle_fwd = fwd_defaults_for(iq.from.stripped)

	REDIS.lindex("catapult_cred-#{iq.from.stripped}", 3).then { |tel|
		raise "Not a registered JID" unless tel

		EMPromise.all([
			REDIS.mget(
				"catapult_fwd-#{tel}",
				"catapult_fwd_timeout-#{iq.from.stripped}"
			),
			REDIS.getbit(
				"catapult_settings_flags-#{iq.from.stripped}",
				CatapultSettingFlagBits::VOICEMAIL_TRANSCRIPTION_DISABLED
			)
		])
	}.then { |((fwd, timeout), transcription_disabled)|
		timeout ||= -1
		fwd_status = if fwd == jingle_fwd
			:jingle
		elsif fwd == default_fwd
			:default
		else
			:custom
		end

		reply = iq.reply
		reply.new_sessionid!
		reply.node = 'configure-calls'
		reply.status = :executing
		# NOTE: will have to change this if we have anything multi-stage
		# (such as when you update your forwarding number)
		reply.allowed_actions = [:complete]
		form = reply.form
		form.title = 'Configure Calls'
		form.fields = [
			xep0122_field(
				'xs:integer',
				var: 'catapult_fwd_timeout',
				type: 'text-single',
				label: 'Seconds to ring before voicemail',
				description:
					'One ring is ~5 seconds. '\
					'Negative means ring forever.',
				value: timeout.to_s
			),
			Blather::Stanza::X::Field.new(
				var: 'voicemail_transcription',
				type: 'boolean',
				label: 'Voicemail transcription',
				value: transcription_disabled == 0 ? "true" : "false"
			)
		] +
			if fwd_status == :custom
				[]
			else
				[Blather::Stanza::X::Field.new(
					var: 'jingle',
					type: 'boolean',
					label: 'Forward calls to XMPP',
					description:
						'Incoming calls will ring '\
						'using Jingle audio in your XMPP client.',
					value: fwd_status == :jingle ? "true" : "false"
				)]
			end
		SGXcatapult.write reply
	}
end

SGXcatapult.before_handler(
	:command,
	node: 'configure-calls',
	sessionid: /./
) do |iq|
	throw :pass if iq.to.node

	promises = []
	timeout = iq.form.field('catapult_fwd_timeout')
	if timeout && timeout.value.to_s != ''
		promises << REDIS.set(
			"catapult_fwd_timeout-#{iq.from.stripped}",
			timeout.value.to_i
		)
	end

	transcription_enabled = iq.form.field('voicemail_transcription')
	if transcription_enabled
		promises << REDIS.setbit(
			"catapult_settings_flags-#{iq.from.stripped}",
			CatapultSettingFlagBits::VOICEMAIL_TRANSCRIPTION_DISABLED,
			xep0004_boolean_value(transcription_enabled) ? 0 : 1
		)
	end

	jingle = iq.form.field('jingle') rescue nil
	unless jingle.nil?
		promises << REDIS.lindex("catapult_cred-#{iq.from.stripped}", 3).then { |tel|
			raise "Not a registered JID" unless tel

			default_fwd, jingle_fwd = fwd_defaults_for(iq.from.stripped)
			REDIS.set(
				"catapult_fwd-#{tel}",
				xep0004_boolean_value(jingle) ? jingle_fwd : default_fwd
			)
		}
	end

	reply = iq.reply
	reply.command[:sessionid] = iq.sessionid
	reply.node = 'configure-calls'
	reply.status = :completed
	note = reply.note
	note.content = 'Configuration saved!'
	note[:type] = :info

	EMPromise.all(promises).then {
		SGXcatapult.write reply
	}
end

SGXcatapult.before_handler(
	:command,
	:execute?,
	node: 'record-voicemail-greeting',
	sessionid: nil
) do |iq|
	throw :pass if iq.to.node

	cred_key = "catapult_cred-#{iq.from.stripped}"
	REDIS.lrange(cred_key, 0, 3).then { |creds|
		fwd_key = "catapult_fwd-#{creds.last}"
		REDIS.get(fwd_key).then { |fwd| [fwd] + v1ify(creds) }
	}.then { |args|
		JmpRecordVoicemailGreeting.new_call(*args, iq.from.stripped)
	}.then {
		reply = iq.reply
		reply.new_sessionid!
		reply.node = 'record-voicemail-greeting'
		reply.status = :completed
		note = reply.note
		note.content = 'You will now receive a call.'
		note[:type] = :info

		SGXcatapult.write reply
	}
end
